package ir.vtj.shiptest.dao.db

import android.content.Context
import android.content.SharedPreferences

class SharedPreferencesHelper(private val context: Context) {
    companion object {
        const val KEY_CONFIG = "config"
        const val KEY_CONFIG_VERSION = "config_version"
        const val KEY_IP_SHIP = "ship_ip"
        const val KEY_PORT_SHIP = "ship_port"
        const val KEY_TIMER_SHIP = "ship_timer"
        const val KEY_GPS_LAT_DIS = "lat_dis"
        const val KEY_GPS_LNG_DIS = "lng_dis"
        const val KEY_TIME_DIS = "time_dis"
    }

    private fun sharedPreferencesLoadObject(key: String, defValue: Any): Any {
        val sharedPref: SharedPreferences = context.getSharedPreferences(context.packageName, 0)

        return when (defValue) {
            is Int -> sharedPref.getInt(key, defValue)
            is Boolean -> sharedPref.getBoolean(key, defValue)
            else -> sharedPref.getString(key, defValue as String)!!
        }

    }

    fun sharedPreferencesLoad(key: String, defValue: String): String {
        return sharedPreferencesLoadObject(key, defValue) as String
    }

    fun sharedPreferencesLoad(key: String, defValue: Int): Int {
        return sharedPreferencesLoadObject(key, defValue) as Int
    }

    fun sharedPreferencesLoad(key: String, defValue: Boolean): Boolean {
        return sharedPreferencesLoadObject(key, defValue) as Boolean
    }

    fun sharedPreferencesSave(key: String, value: Any) {
        val sharedPref: SharedPreferences = context.getSharedPreferences(context.packageName, 0)
        val editor = sharedPref.edit()
        when (value) {
            is String -> editor.putString(key, value)
            is Int -> editor.putInt(key, value)
            is Boolean -> editor.putBoolean(key, value)
        }
        editor.apply()
    }


}

package ir.vtj.shiptest.dao.entity

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import ir.vtj.shiptest.R


@Entity(tableName = "ship")
data class Ship(
    @NonNull
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var source: String,
    var shipName: String,
    var lng: String,
    var lat: String,
    var alt: String,
    var date: String,
    var satellite: String,
    var status: String,
    var speed: String,
    var way: String,
    var password: String
) {
    companion object {
        const val STATUS_NORMAL = "normal"
        const val STATUS_NORMAL_TEXT = "عادی"
        const val STATUS_NORMAL_COLOR = R.color.green

        const val STATUS_INIT = "init"
        const val STATUS_INIT_TEXT = "راه افتاده"
        const val STATUS_INIT_COLOR = R.color.yellow

        const val STATUS_DAMAGED = "damaged"
        const val STATUS_DAMAGED_TEXT = "صدمه دیده"
        const val STATUS_DAMAGED_COLOR = R.color.red

        const val STATUS_STOP = "stop"
        const val STATUS_STOP_TEXT = "متوقف"
        const val STATUS_STOP_COLOR = R.color.gray
    }


    fun getValue(index: Int): Any {

        return when (index) {
            1 -> source
            2 -> shipName
            3 -> lng
            4 -> lat
            5 -> alt
            6 -> date
            7 -> satellite
            8 -> status
            9 -> speed
            10 -> way
            11 -> password
            12 -> "end"
            else -> shipName
        }
    }

    fun getColor(): Int {
        return when (status) {
            STATUS_NORMAL -> STATUS_NORMAL_COLOR
            STATUS_INIT -> STATUS_INIT_COLOR
            STATUS_DAMAGED -> STATUS_DAMAGED_COLOR
            STATUS_STOP -> STATUS_STOP_COLOR

            else -> STATUS_NORMAL_COLOR
        }
    }

    private fun Double.format(digits: Int) = "%.${digits}f".format(this)

    fun getDateString(drift: String): String {
        //2020 04 06    16 36 25
        return if (date.length == 14) {
            val newDate = (date.toDouble() - drift.toDouble()).format(0)
            val year = newDate.substring(0, 4)
            val month = newDate.substring(4, 6)
            val day = newDate.substring(6, 8)
            val hour = newDate.substring(8, 10)
            val minute = newDate.substring(10, 12)
            val sec = newDate.substring(12, 14)
            "$year/$month/$day $hour:$minute:$sec"
        } else {
            date
        }

    }
}


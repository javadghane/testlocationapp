package ir.vtj.shiptest.dao.db

import androidx.lifecycle.LiveData
import androidx.room.*
import ir.vtj.shiptest.dao.entity.Ship
import java.util.*

@Dao
interface AppDao {

    @Query("DELETE FROM ship WHERE id = :id ")
    fun deleteShip(id: Int)

    @Query("SELECT * from ship ORDER BY CAST(id AS INTEGER) DESC")
    fun getShipsLive(): LiveData<List<Ship>>

    @Query("SELECT * from ship  ORDER BY CAST(id AS INTEGER) DESC")
    fun getShips(): List<Ship>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertShip(data: Ship)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun bulkInsertShip(data: ArrayList<Ship>)

    @Update
    fun updateShip(data: Ship)
}

package ir.vtj.shiptest.dao.entity

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ir.vtj.shiptest.App
import ir.vtj.shiptest.dao.db.SharedPreferencesHelper
import java.util.*


data class Config(
    val title: String,
    val code: String,
    val order: Int,
    val defOrder: Int
) {
    companion object {

        private fun getDefaultConfigJson(): String {
            val configs: ArrayList<Config> = arrayListOf()
            configs.add(Config("منبع داده", "src", 1, 1))
            configs.add(Config("نام کشتی", "dvc", 2, 2))
            configs.add(Config("عرض جغرافیایی", "lng", 3, 3))
            configs.add(Config("طول جغرافیایی", "lat", 4, 4))
            configs.add(Config("ارتفاع", "alt", 5, 5))
            configs.add(Config("تاریخ و زمان", "dat", 6, 6))
            configs.add(Config("تعداد ماهواره", "sat", 7, 7))
            configs.add(Config("وضعیت کشتی", "sts", 8, 8))
            configs.add(Config("سرعت", "spd", 9, 9))
            configs.add(Config("جهت کشتی", "way", 10, 10))
            configs.add(Config("رمز هویتی", "psw", 11, 11))
            configs.add(Config("پایان پیام", "end", 12, 12))
            return Gson().toJson(configs)
        }

        fun getConfig(context: Context): ArrayList<Config> {
            val configVersion = SharedPreferencesHelper(context).sharedPreferencesLoad(SharedPreferencesHelper.KEY_CONFIG_VERSION, 0)
            val configDataJson = if (configVersion < App.configVersion) {
                getDefaultConfigJson()
            } else {
                SharedPreferencesHelper(context).sharedPreferencesLoad(SharedPreferencesHelper.KEY_CONFIG, getDefaultConfigJson())
            }
            val listType = object : TypeToken<List<Config>>() {}.type
            val data: ArrayList<Config> = Gson().fromJson(configDataJson, listType)
            data.sortBy { it.order }
            return data
        }

        fun saveConfig(context: Context, configs: ArrayList<Config>) {
            SharedPreferencesHelper(context).sharedPreferencesSave(SharedPreferencesHelper.KEY_CONFIG, Gson().toJson(configs))
            SharedPreferencesHelper(context).sharedPreferencesSave(SharedPreferencesHelper.KEY_CONFIG_VERSION, App.configVersion)
        }
    }
}
package ir.vtj.shiptest.view.setting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import ir.vtj.shiptest.R


@Suppress("DEPRECATION")
class FragmentSetting : DialogFragment() {

    private lateinit var delegate: Interaction


    interface Interaction {
        fun onChangeOrder()
    }

    companion object {
        fun newInstance(delegate: Interaction): FragmentSetting {
            val frag = FragmentSetting()
            val args = Bundle()
            frag.arguments = args
            frag.delegate = delegate
            return frag
        }

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_config, container, false)



        return view
    }


}
package ir.vtj.shiptest.view.config


import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ir.vtj.shiptest.R
import ir.vtj.shiptest.dao.entity.Config
import ir.vtj.shiptest.helper.ui.ItemTouchHelperAdapter
import kotlinx.android.synthetic.main.adapter_config.view.*
import java.util.*


/**
 * Created by JavaDroid on 7/18/2015.
 */
class AdapterConfig(
    private val configs: ArrayList<Config>,
    private val delegate: Interaction

) : RecyclerView.Adapter<AdapterConfig.MainHolder>(), ItemTouchHelperAdapter {

    interface Interaction {
        fun click(data: Config, position: Int)
        fun onLongClick(data: Config, position: Int)
        fun onPositionChanged(oldPosition: Int, newPosition: Int)
    }

    fun getItems(): ArrayList<Config> {
        return configs
    }

    override fun getItemCount(): Int {
        return configs.size
    }


    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(mainHolder: MainHolder, position: Int) {
        val conf = configs[position]

        mainHolder.tvSharp.text = (position + 1).toString()
        mainHolder.tvData.text = conf.title + "(" + conf.code + ")"
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MainHolder {
        val itemView = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.adapter_config, viewGroup, false)
        return MainHolder(itemView)
    }

    class MainHolder(v: View) : RecyclerView.ViewHolder(v) {

        // var relMain: RelativeLayout = v.relMain
        //var mainCard: CardView = v.mainCard
        var tvData: TextView = v.tvData
        var tvSharp: TextView = v.tvSharp


    }

    override fun onItemDismiss(position: Int) {
        configs.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int) {
        Collections.swap(configs, fromPosition, toPosition)

        // val prev: Config = configs.removeAt(fromPosition)
        // configs.add(if (toPosition > fromPosition) toPosition - 1 else toPosition, prev)
        delegate.onPositionChanged(fromPosition, toPosition)
        notifyItemMoved(fromPosition, toPosition)

    }


}

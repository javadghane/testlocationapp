package ir.vtj.shiptest.view.start

import android.content.Context
import android.net.wifi.WifiManager
import android.os.Bundle
import android.text.format.Formatter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import ir.vtj.shiptest.R
import kotlinx.android.synthetic.main.fragment_starter.view.*

@Suppress("DEPRECATION")
class FragmentStarter : DialogFragment() {

    private lateinit var delegate: Interaction

    interface Interaction {
        fun onConnect(ip: String, port: Int)
        fun onError(msg: String)
    }

    companion object {
        fun newInstance(delegate: Interaction): FragmentStarter {
            val frag = FragmentStarter()
            val args = Bundle()
            frag.arguments = args
            frag.delegate = delegate
            return frag
        }

    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(
            R.layout.fragment_starter,
            container, false
        )

        val wm = activity!!.application.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val socketIpAddress = Formatter.formatIpAddress(wm.connectionInfo.ipAddress)
        view.tvIp.text = socketIpAddress

        view.tvCreateClient.setOnClickListener {
            val port = view.edtPort.text.toString().toInt()
            delegate.onConnect(socketIpAddress, port)
            dismiss()
        }





        return view
    }


}
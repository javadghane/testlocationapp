package ir.vtj.shiptest.view.operator

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import ir.vtj.shiptest.App
import ir.vtj.shiptest.R
import ir.vtj.shiptest.dao.entity.Ship
import ir.vtj.shiptest.helper.socket.HelperSocket
import ir.vtj.shiptest.view.config.FragmentConfig
import ir.vtj.shiptest.view.map.FragmentGoogleMap
import ir.vtj.shiptest.view.map.FragmentNeshanMap
import kotlinx.android.synthetic.main.activity_operator.*


class ActivityOprator : AppCompatActivity() {


    companion object {
        const val KEY_PORT = "port"
        const val KEY_IP = "ip"
    }

    lateinit var fragmentListData: FragmentListData
    private var isConnect = false
    private lateinit var helperSocket: HelperSocket
    private var ipAddress = ""
    private var portAddress = 8080
    private var fullAddress = ""

    lateinit var frgMapGoogle: FragmentGoogleMap
    lateinit var frgMapNeshan: FragmentNeshanMap

    private val mapGoogle = 1
    private val mapNeshan = 2
    private var currentMapType = mapGoogle
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_operator)
        ipAddress = intent.extras!!.getString(KEY_IP, "")
        portAddress = intent.extras!!.getInt(KEY_PORT, 8080)

        connect(ipAddress, portAddress)

        frgMapGoogle = FragmentGoogleMap.newInstance(object : FragmentGoogleMap.Interaction {
            override fun onMapLoad() {
            }
        })
        supportFragmentManager.beginTransaction().replace(R.id.frmContainerMapGoogle, frgMapGoogle).commit()


        frgMapNeshan = FragmentNeshanMap.newInstance(object : FragmentNeshanMap.Interaction {
            override fun onMapLoad() {

            }
        })
        supportFragmentManager.beginTransaction().replace(R.id.frmContainerMapNeshan, frgMapNeshan).commit()


        tvMap.setOnClickListener {
            if (App.isMapShow) {
                hideMap()
                tvMap.text = getString(R.string.shomap)
            } else {
                showMap()
                tvMap.text = getString(R.string.backToList)
                tvGoogle.setOnClickListener {
                    currentMapType = mapGoogle
                    showMap()
                }
                tvNeshan.setOnClickListener {
                    currentMapType = mapNeshan
                    showMap()
                }
            }
        }
    }

    private fun hideMap() {
        App.isMapShow = false
        frmContainerMapGoogle.visibility = View.GONE
        frmContainerMapNeshan.visibility = View.GONE
        tvGoogle.visibility = View.GONE
        tvNeshan.visibility = View.GONE

        frmContainer.visibility = View.VISIBLE
    }

    private fun showMap() {
        App.isMapShow = true
        tvGoogle.visibility = View.VISIBLE
        tvNeshan.visibility = View.VISIBLE

        if (currentMapType == mapGoogle) {
            tvGoogle.isEnabled = false
            tvNeshan.isEnabled = true
            frmContainerMapGoogle.visibility = View.VISIBLE
            frmContainerMapNeshan.visibility = View.GONE
        } else if (currentMapType == mapNeshan) {
            tvGoogle.isEnabled = true
            tvNeshan.isEnabled = false
            frmContainerMapNeshan.visibility = View.VISIBLE
            frmContainerMapGoogle.visibility = View.GONE
        }

        frmContainer.visibility = View.GONE
    }

    private fun connect(ip: String, port: Int) {
        if (isConnect) {
            isConnect = false
            if (::helperSocket.isInitialized)
                helperSocket.disconnect()
        } else {
            helperSocket = HelperSocket().initServer(port)
            helperSocket.createServer(object : HelperSocket.SocketListener {
                override fun onConnect() {
                    val address = getString(R.string.serverCreated) + ip + ":" + port.toString()
                    isConnect = true

                    runOnUiThread {
                        App.toaster("Connected", this@ActivityOprator)

                        fullAddress = address
                        showList()
                    }
                }

                override fun onError(error: String) {
                    runOnUiThread {
                        App.toaster(error, this@ActivityOprator)
                    }
                }

                override fun onRead(msg: String) {
                    runOnUiThread {
                        fragmentListData.addShipData(msg)
                    }
                }

                override fun onDisconnect() {
                    runOnUiThread {
                        App.toaster("Disconnected", this@ActivityOprator)
                        onBackPressed()
                    }
                }
            })

        }
    }

    fun showList() {
        App.isMapShow = false

        fragmentListData = FragmentListData.newInstance(fullAddress, object : FragmentListData.Interaction {
            override fun onConfig() {
                showConfig()
            }

            override fun onClear() {
                if (::frgMapGoogle.isInitialized) {
                    frgMapGoogle.clear()
                }
                if (::frgMapNeshan.isInitialized) {
                    frgMapNeshan.clear()
                }
            }

            override fun onError(msg: String) {
                runOnUiThread {
                    App.toaster(msg, this@ActivityOprator)
                }
            }

            override fun onMarkerClick(ship: Ship, position: Int) {
                showMapMarker(ship, position.toLong())
            }

            override fun onPathClick(ship: Ship, position: Int) {
                showMapMarker(ship, position.toLong())
            }

            override fun onLoadNewShip(ship: Ship, position: Int) {
                showMapMarker(ship, position.toLong())
            }
        })

        supportFragmentManager.beginTransaction().replace(R.id.frmContainer, fragmentListData).commit()
    }

    fun showMapMarker(ship: Ship, position: Long) {
        if (::frgMapGoogle.isInitialized) {
            showMap()
            frgMapGoogle.addMarker(ship.lat.toDouble(), ship.lng.toDouble(), ship.shipName, ship.getColor(), position)
        }

        if (::frgMapNeshan.isInitialized) {
            frgMapNeshan.addMarker(ship.lat.toDouble(), ship.lng.toDouble(), ship.shipName, ship.getColor(), position)
        }
    }

    fun showConfig() {
        val frgConfig = FragmentConfig.newInstance(object : FragmentConfig.Interaction {
            override fun onChangeOrder() {
                App.toaster(getString(R.string.configSaved), this@ActivityOprator)
            }
        })

        frgConfig.show(supportFragmentManager, "config")
    }

    override fun onBackPressed() {
        when {
            App.isMapShow -> {
                hideMap()
            }
            isConnect -> {
                helperSocket.disconnect()
                isConnect = false
                super.onBackPressed()
            }
            else -> {
                super.onBackPressed()
            }
        }

    }
}
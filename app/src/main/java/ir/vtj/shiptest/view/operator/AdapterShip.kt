package ir.vtj.shiptest.view.operator


import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import ir.vtj.shiptest.R
import ir.vtj.shiptest.dao.db.SharedPreferencesHelper
import ir.vtj.shiptest.dao.entity.Ship
import kotlinx.android.synthetic.main.adapter_ship.view.*


/**
 * Created by JavaDroid on 7/18/2015.
 */
class AdapterShip(
    private val context: Context,
    private val animFadeIn: Animation,
    private val ships: ArrayList<Ship>,
    private val delegate: Interaction

) : RecyclerView.Adapter<AdapterShip.MainHolder>() {


    interface Interaction {
        fun click(ship: Ship, position: Int)
        fun onMarkerClick(ship: Ship, position: Int)
        fun onPathClick(ship: Ship, position: Int)
    }


    override fun getItemCount(): Int {
        return ships.size
    }


    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(mainHolder: MainHolder, position: Int) {
        val ship = ships[position]

        mainHolder.mainCard.setBackgroundColor(ContextCompat.getColor(context, R.color.gray_back))
        mainHolder.mainCard.startAnimation(animFadeIn)
        android.os.Handler().postDelayed({
            mainHolder.mainCard.setBackgroundColor(Color.WHITE)
        }, 1000)

        mainHolder.relMain.setOnClickListener {
            delegate.click(ship, position)
        }

        mainHolder.imgMarker.setOnClickListener {
            delegate.onMarkerClick(ship, position)
        }
        mainHolder.imgPath.setOnClickListener {
            delegate.onPathClick(ship, position)
        }

        mainHolder.tvShipName.text = "نام:" + ship.shipName
        when (ship.status) {
            Ship.STATUS_NORMAL -> {
                mainHolder.tvStatus.text = "وضعیت:" + Ship.STATUS_NORMAL_TEXT
                mainHolder.tvStatus.setBackgroundColor(Color.GREEN)
            }
            Ship.STATUS_INIT -> {
                mainHolder.tvStatus.text = "وضعیت:" + Ship.STATUS_INIT_TEXT
                mainHolder.tvStatus.setBackgroundColor(Color.YELLOW)
            }
            Ship.STATUS_STOP -> {
                mainHolder.tvStatus.text = "وضعیت:" + Ship.STATUS_STOP_TEXT
                mainHolder.tvStatus.setBackgroundColor(Color.LTGRAY)
            }
            Ship.STATUS_DAMAGED -> {
                mainHolder.tvStatus.text = "وضعیت:" + Ship.STATUS_DAMAGED_TEXT
                mainHolder.tvStatus.setBackgroundColor(Color.RED)
            }
            else -> {
                mainHolder.tvStatus.text = "وضعیت:" + ship.status
                mainHolder.tvStatus.setBackgroundColor(Color.WHITE)
            }
        }

        val timeDrift = SharedPreferencesHelper(context).sharedPreferencesLoad(SharedPreferencesHelper.KEY_TIME_DIS, "0")
        mainHolder.tvDate.text = ship.getDateString(timeDrift)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MainHolder {
        val itemView = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.adapter_ship, viewGroup, false)
        return MainHolder(itemView)
    }

    class MainHolder(v: View) : RecyclerView.ViewHolder(v) {

        var relMain: RelativeLayout = v.relMain
        var mainCard: CardView = v.mainCard
        var tvShipName: TextView = v.tvShipName
        var tvStatus: TextView = v.tvStatus
        var tvDate: TextView = v.tvDate
        var imgMarker: ImageView = v.imgMarker
        var imgPath: ImageView = v.imgPath


    }
}

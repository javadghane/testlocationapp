package ir.vtj.shiptest.view.config

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import ir.vtj.shiptest.R
import ir.vtj.shiptest.dao.entity.Config
import ir.vtj.shiptest.helper.ui.SimpleItemTouchHelperCallback
import kotlinx.android.synthetic.main.fragment_config.view.*
import kotlinx.android.synthetic.main.fragment_list.view.recMain


@Suppress("DEPRECATION")
class FragmentConfig : DialogFragment() {

    private lateinit var delegate: Interaction
    private lateinit var adapterConfig: AdapterConfig
    private var configs: ArrayList<Config> = arrayListOf()


    interface Interaction {
        fun onChangeOrder()
    }

    companion object {
        fun newInstance(delegate: Interaction): FragmentConfig {
            val frag = FragmentConfig()
            val args = Bundle()
            frag.arguments = args
            frag.delegate = delegate
            return frag
        }

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_config, container, false)

        configs.clear()
        configs.addAll(Config.getConfig(activity!!))
        adapterConfig = AdapterConfig(configs, object : AdapterConfig.Interaction {
            override fun click(data: Config, position: Int) {

            }

            override fun onLongClick(data: Config, position: Int) {

            }

            override fun onPositionChanged(oldPosition: Int, newPosition: Int) {

            }
        })

        view.recMain.layoutManager = LinearLayoutManager(activity!!)
        view.recMain.adapter = adapterConfig


        val callback: ItemTouchHelper.Callback = SimpleItemTouchHelperCallback(adapterConfig)
        val mItemTouchHelper = ItemTouchHelper(callback)
        mItemTouchHelper.attachToRecyclerView(view.recMain)


        view.tvSave.setOnClickListener {
            val configsForSave: ArrayList<Config> = arrayListOf()
            val items = adapterConfig.getItems()
            for (i in 0 until items.size) {
                configsForSave.add(items[i].copy(order = i + 1))
            }
            Config.saveConfig(activity!!, configsForSave)
            delegate.onChangeOrder()
            dismiss()
        }

        return view
    }


}
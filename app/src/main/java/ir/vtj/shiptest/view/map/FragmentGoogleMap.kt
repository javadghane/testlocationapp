package  ir.vtj.shiptest.view.map


import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.*
import ir.vtj.shiptest.R
import ir.vtj.shiptest.dao.db.SharedPreferencesHelper
import kotlinx.android.synthetic.main.custom_marker_image.view.*
import kotlinx.android.synthetic.main.fragment_map_google.view.*
import kotlin.math.floor


class FragmentGoogleMap : Fragment() {


    private lateinit var delegate: Interaction
    private var markers: ArrayList<Marker> = arrayListOf()
    private lateinit var polyline: Polyline
    private var isLoadMap: Boolean = false
    private lateinit var googleMap: GoogleMap

    interface Interaction {
        fun onMapLoad()
    }

    companion object {
        fun newInstance(delegate: Interaction): FragmentGoogleMap {
            val frag = FragmentGoogleMap()
            val args = Bundle()
            frag.arguments = args
            frag.delegate = delegate
            return frag
        }

    }

    fun clear() {
        if (::polyline.isInitialized) {
            polyline.remove()
        }
        googleMap.clear()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(
            R.layout.fragment_map_google,
            container, false
        )

        view.clear.setOnClickListener {
            clear()
        }


        view.mMapView.onCreate(savedInstanceState)
        view.mMapView.onResume()

        MapsInitializer.initialize(activity?.applicationContext)


        view.mMapView.getMapAsync { gMap ->
            isLoadMap = true
            googleMap = gMap
            //googleMap.isTrafficEnabled = true
            delegate.onMapLoad()
            changeCenter(LatLng(35.6892, 51.3890)) //Tehran


            googleMap.setOnInfoWindowClickListener { marker ->
                val p = MapPoint(marker.position.latitude, marker.position.longitude)
                // App.toaster(p.ship.shipName, activity!!)

            }

            googleMap.setOnMapClickListener {

            }
        }


        return view
    }


    fun addMarker(lat: Double, lng: Double, title: String, color: Int, position: Long) {

        val latDis = SharedPreferencesHelper(activity!!).sharedPreferencesLoad(SharedPreferencesHelper.KEY_GPS_LAT_DIS, "0").toDouble()
        val lngDis = SharedPreferencesHelper(activity!!).sharedPreferencesLoad(SharedPreferencesHelper.KEY_GPS_LNG_DIS, "0").toDouble()

        val newLat = lat - latDis
        val newLng = lng - lngDis

        val location = LatLng(newLat, newLng)
        val marker = MarkerOptions().position(location)
            .icon(BitmapDescriptorFactory.fromBitmap(getNormalMarkerBitmap(color)))
            .title(title)

        //val p = MapPoint(lat, lng)

        val addedMarker = googleMap.addMarker(marker)//.showInfoWindow();
        addedMarker.tag = position
        markers.add(addedMarker)
        changeCenter(location)

    }

    @SuppressLint("InflateParams")
    private fun getNormalMarkerBitmap(color: Int): Bitmap {
        val customMarker = this.layoutInflater.inflate(R.layout.custom_marker_image, null, false)
        /* val tv = customMarker.tv
         tv.text = item.title*/
        val img = customMarker.imgIcon
        img.setColorFilter(ContextCompat.getColor(activity!!, color), android.graphics.PorterDuff.Mode.SRC_IN)


        //ImageViewCompat.setImageTintList(img, ColorStateList.valueOf(color));

        //Glide.with(activity!!.applicationContext).load(WebServiceFactory.SERVER_PATH_IMAGE_ITEM + item.avatar).error(R.drawable.no_pic).placeholder(R.drawable.load_img).into(img)

        customMarker.measure(
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        )
        customMarker.layout(0, 0, customMarker.measuredWidth, customMarker.measuredHeight)
        customMarker.isDrawingCacheEnabled = true
        customMarker.buildDrawingCache()
        return customMarker.drawingCache
    }


    /*   private fun drawRoutingLine(neshanRouting: MainRoutes) {
           val lineArray = arrayListOf<LatLng>()
           for (route in neshanRouting.routing) {
               val linePoints = PolyUtil.decode(route.line)
               for (i in linePoints.indices) {
                   lineArray.add(LatLng(linePoints[i].latitude, linePoints[i].longitude))
               }
           }
           val polylineOptions = PolylineOptions()
           for (linePoint in lineArray) {
               polylineOptions.add(linePoint)
           }
           polylineOptions.width(20.toFloat())
           polylineOptions.color(Color.RED)
           polyline = googleMap.addPolyline(polylineOptions)

       }
   */

    private fun changeCenter(focalPoint: LatLng) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(focalPoint, 17f))

        //map.setFocalPointPosition(focalPoint, 0f)
    }


    class MapPoint internal constructor(var X: Double, var Y: Double) {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other == null || javaClass != other.javaClass) return false
            val mapPoint = other as MapPoint?
            return mapPoint!!.X.compareTo(X) == 0 && mapPoint.Y.compareTo(Y) == 0
        }


        override fun hashCode(): Int {
            return floor(X + Y).toInt()
        }
    }
}
package ir.vtj.shiptest.view.ship

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import ir.vtj.shiptest.App
import ir.vtj.shiptest.R
import ir.vtj.shiptest.dao.db.SharedPreferencesHelper
import ir.vtj.shiptest.dao.entity.Config
import ir.vtj.shiptest.dao.entity.Ship
import ir.vtj.shiptest.helper.CounterDownerTimer
import ir.vtj.shiptest.helper.Utils
import ir.vtj.shiptest.helper.gps.HelperGPS
import ir.vtj.shiptest.helper.socket.HelperSocket
import ir.vtj.shiptest.view.config.FragmentConfig
import kotlinx.android.synthetic.main.activity_ship.*
import java.util.*


class ActivityShip : AppCompatActivity() {

    lateinit var helperSocket: HelperSocket
    lateinit var helperGPS: HelperGPS

    lateinit var ship: Ship

    private var isShowTime = true
    lateinit var timer: CounterDownerTimer

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ship)

        ship = Ship(0, "GPS", edtName.text.toString(), (0 + edtGpsLngDrift.text.toString().toDouble()).toString(), (0 + edtGpsLatDrift.text.toString().toDouble()).toString(), "0", ("20200401102030".toDouble() + edtTimeDrift.text.toString().toDouble()).toString(), "0", "normal", "0", "0", edtPass.text.toString())


        edtIp.setText(SharedPreferencesHelper(this@ActivityShip).sharedPreferencesLoad(SharedPreferencesHelper.KEY_IP_SHIP, "192.168.1."))
        edtPort.setText(SharedPreferencesHelper(this@ActivityShip).sharedPreferencesLoad(SharedPreferencesHelper.KEY_PORT_SHIP, "8080"))
        edtTime.setText(SharedPreferencesHelper(this@ActivityShip).sharedPreferencesLoad(SharedPreferencesHelper.KEY_TIMER_SHIP, "20"))
        edtGpsLatDrift.setText(SharedPreferencesHelper(this@ActivityShip).sharedPreferencesLoad(SharedPreferencesHelper.KEY_GPS_LAT_DIS, "0"))
        edtGpsLngDrift.setText(SharedPreferencesHelper(this@ActivityShip).sharedPreferencesLoad(SharedPreferencesHelper.KEY_GPS_LNG_DIS, "0"))
        edtTimeDrift.setText(SharedPreferencesHelper(this@ActivityShip).sharedPreferencesLoad(SharedPreferencesHelper.KEY_TIME_DIS, "0"))


        tvDataSend.setOnLongClickListener {
            tvDataSend.text = ""
            return@setOnLongClickListener true
        }

        startGPS()
        gpsStatus.setOnClickListener {
            startGPS()
        }

        Handler(Looper.getMainLooper()).postDelayed({
            setTimer()
        }, 1000)

        btnConfig.setOnClickListener {
            showConfig()
        }

        btnStart.setOnClickListener {
            SharedPreferencesHelper(this@ActivityShip).sharedPreferencesSave(SharedPreferencesHelper.KEY_IP_SHIP, edtIp.text.toString())
            SharedPreferencesHelper(this@ActivityShip).sharedPreferencesSave(SharedPreferencesHelper.KEY_PORT_SHIP, edtPort.text.toString())
            SharedPreferencesHelper(this@ActivityShip).sharedPreferencesSave(SharedPreferencesHelper.KEY_TIMER_SHIP, edtTime.text.toString())
            SharedPreferencesHelper(this@ActivityShip).sharedPreferencesSave(SharedPreferencesHelper.KEY_GPS_LAT_DIS, edtGpsLatDrift.text.toString())
            SharedPreferencesHelper(this@ActivityShip).sharedPreferencesSave(SharedPreferencesHelper.KEY_GPS_LNG_DIS, edtGpsLngDrift.text.toString())
            SharedPreferencesHelper(this@ActivityShip).sharedPreferencesSave(SharedPreferencesHelper.KEY_TIME_DIS, edtTimeDrift.text.toString())

            helperSocket = HelperSocket().initClient(edtIp.text.toString(), edtPort.text.toString().toInt()).connect(object : HelperSocket.SocketListener {
                override fun onConnect() {
                    Utils.log("onConnect")
                    runOnUiThread {
                        App.toaster("Connected", this@ActivityShip)
                        btnStop.isEnabled = true
                        btnStart.isEnabled = false
                        edtIp.isEnabled = false
                        edtTime.isEnabled = false
                        edtPort.isEnabled = false
                        sendData()
                    }


                }

                override fun onError(error: String) {
                    Utils.log("onError:$error")
                    runOnUiThread {
                        App.toaster(error, this@ActivityShip)
                    }

                }

                override fun onRead(msg: String) {
                    Utils.log("onRead: $msg")
                }

                override fun onDisconnect() {
                    Utils.log("onDisconnect")
                    runOnUiThread {

                        App.toaster("Disconnected", this@ActivityShip)
                    }

                }
            })
        }

        btnStop.setOnClickListener {

            helperSocket.disconnect()
            btnStart.isEnabled = true
            btnStop.isEnabled = false

            edtIp.isEnabled = true
            edtPort.isEnabled = true
            edtTime.isEnabled = true

            if (::timer.isInitialized)
                timer.cancel()

        }
    }


    @SuppressLint("SetTextI18n")
    private fun sendData() {
        val shipForSend = ship.copy()

        ship.shipName = edtName.text.toString()
        ship.password = edtPass.text.toString()

        when {
            rdDamaged.isChecked -> {
                ship.status = "damaged"
            }
            rdStop.isChecked -> {
                ship.status = "stop"
            }
            rdNormal.isChecked -> {
                ship.status = "normal"
            }
            rdInit.isChecked -> {
                ship.status = "init"
            }
        }

        val time: Long = edtTime.text.toString().toLong() * 1000
        Handler(Looper.getMainLooper()).postDelayed({
            if (helperSocket.isConnect()) {
                sendData()
            } else {
                helperSocket.disconnect()
                btnStart.isEnabled = true
                btnStop.isEnabled = false
            }
        }, time)


        val configs: ArrayList<Config> = arrayListOf()
        configs.addAll(Config.getConfig(this@ActivityShip))
        configs.sortBy { it.order }



        shipForSend.lat = (ship.lat.toDouble() + edtGpsLatDrift.text.toString().toDouble()).toString()
        shipForSend.lng = (ship.lng.toDouble() + edtGpsLngDrift.text.toString().toDouble()).toString()

        val paramSorted: ArrayList<String> = arrayListOf()

        for (conf in configs) {
            paramSorted.add(ship.getValue(conf.defOrder).toString())
        }

        val sb = StringBuilder()
        for (data in paramSorted) {
            sb.append(data)
            sb.append(",")
        }

        sb.deleteCharAt(sb.length - 1)

        val output = sb.toString()
        tvDataSend.text = tvDataSend.text.toString() + "\n\n" + "Sent: \n $output"

        showCountdown(time - 1000)

        helperSocket.sendDataFromClient(output)
    }

    private fun showCountdown(count: Long) {
        timer = object : CounterDownerTimer(count, 1000) {
            @SuppressLint("SetTextI18n")
            override fun onTick(millisUntilFinished: Long) {
                val time = millisUntilFinished / 1000
                if (time == 1.toLong()) {
                    tvDataSendTimer.text = "آماده ارسال داده ها"
                } else {
                    tvDataSendTimer.text = "$time ثانیه تا ارسال بعدی باقی مانده است"
                }

            }

            override fun onFinish() {

            }
        }
        timer.start()
    }

    private fun showConfig() {
        val frgConfig = FragmentConfig.newInstance(object : FragmentConfig.Interaction {
            override fun onChangeOrder() {
                App.toaster(getString(R.string.configSaved), this@ActivityShip)
            }
        })

        frgConfig.show(supportFragmentManager, "config")
    }

    private fun givePermission() {
        Dexter.withActivity(this@ActivityShip)
            .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    startGPS()
                }

                override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                    token!!.continuePermissionRequest()
                }


            }).check()
    }

    fun startGPS() {
        if (::helperGPS.isInitialized) {
            helperGPS.init(this, gpsListener)
        } else {
            helperGPS = HelperGPS().init(this, gpsListener)
        }


    }

    private val gpsListener = object : HelperGPS.OnGpsDataChange {

        override fun onLocationChange(lat: String, lng: String, alt: String, speed: String, way: String) {
            setGpsStatus("مکان بروزشد:Lat=$lat Lng=$lng")
            ship.lat = lat
            ship.lng = lng
            ship.alt = alt
            ship.speed = speed
            ship.way = way
        }

        override fun onSatteliteCountChange(satteliteCount: String) {
            ship.satellite = satteliteCount
        }

        override fun onPermissionDenied() {
            setGpsStatus("عدم دسترسی")
            givePermission()
        }

        override fun onNoGpsAvailable() {
            setGpsStatus("جی پی اس فعال نیست")
        }

        override fun onGpsAvailable() {
            setGpsStatus("جی پی اس فعال شد")
        }

        override fun onErrorGps(error: String) {
            App.toaster(error, this@ActivityShip)
        }

    }

    @SuppressLint("SetTextI18n")
    fun setGpsStatus(msg: String) {
        gpsStatus.text = "وضعیت GPS: $msg"
    }

    private fun Double.format(digits: Int) = "%.${digits}f".format(this)


    @SuppressLint("SetTextI18n")
    fun setTimer() {
        val calendar = Calendar.getInstance()
        val y = calendar.get(Calendar.YEAR).toString()
        val m = String.format("%02d", calendar.get(Calendar.MONTH) + 1)
        val d = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH))
        val h = String.format("%02d", calendar.get(Calendar.HOUR))
        val mm = String.format("%02d", calendar.get(Calendar.MINUTE))
        val s = String.format("%02d", calendar.get(Calendar.SECOND))

        val timeDrift = SharedPreferencesHelper(this).sharedPreferencesLoad(SharedPreferencesHelper.KEY_TIME_DIS, "0")
        val orgTime = "$y$m$d$h$mm$s"
        ship.date = (orgTime.toDouble() + timeDrift.toDouble()).format(0)

         timeStatus.text = "زمان جاری: $y/$m/$d $h:$mm:$s"


        Handler(Looper.getMainLooper()).postDelayed({
            if (isShowTime)
                setTimer()
        }, 1000)
    }

    override fun onBackPressed() {
        isShowTime = false
        super.onBackPressed()

    }

}
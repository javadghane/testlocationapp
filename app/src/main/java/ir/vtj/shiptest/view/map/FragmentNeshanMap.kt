package  ir.vtj.shiptest.view.map


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import ir.vtj.shiptest.R
import ir.vtj.shiptest.dao.db.SharedPreferencesHelper
import kotlinx.android.synthetic.main.fragment_map_neshan.*
import kotlinx.android.synthetic.main.fragment_map_neshan.view.*
import org.neshan.core.Bounds
import org.neshan.core.LngLat
import org.neshan.core.Range
import org.neshan.graphics.ARGB
import org.neshan.layers.VectorElementEventListener
import org.neshan.layers.VectorElementLayer
import org.neshan.services.NeshanMapStyle
import org.neshan.services.NeshanServices
import org.neshan.styles.*
import org.neshan.ui.ClickType
import org.neshan.ui.ElementClickData
import org.neshan.ui.MapEventListener
import org.neshan.utils.BitmapUtils
import org.neshan.vectorelements.Marker


class FragmentNeshanMap : Fragment() {


    private lateinit var delegate: Interaction
    private lateinit var markerLayer: VectorElementLayer
    private lateinit var userMarkerLayer: VectorElementLayer

    private var isLoadMap: Boolean = false

    private lateinit var lineLayer: VectorElementLayer

    interface Interaction {
        fun onMapLoad()
    }

    companion object {
        fun newInstance(delegate: Interaction): FragmentNeshanMap {
            val frag = FragmentNeshanMap()
            val args = Bundle()
            frag.arguments = args
            frag.delegate = delegate
            return frag
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(
            R.layout.fragment_map_neshan,
            container, false
        )

        val map = view.map



        markerLayer = NeshanServices.createVectorElementLayer()
        userMarkerLayer = NeshanServices.createVectorElementLayer()
        lineLayer = NeshanServices.createVectorElementLayer()
        map.layers.add(markerLayer)
        map.layers.add(userMarkerLayer)
        map.layers.add(lineLayer)
        map.options.setZoomRange(Range(4.5f, 18f))
        map.layers.insert(0, NeshanServices.createBaseMap(NeshanMapStyle.STANDARD_DAY))

        markerLayer.vectorElementEventListener = object : VectorElementEventListener() {
            override fun onVectorElementClicked(clickInfo: ElementClickData?): Boolean {
                if (clickInfo!!.clickType == ClickType.CLICK_TYPE_SINGLE) {
                    val marker = clickInfo.vectorElement as Marker
                    /*  val latlng = items[marker.id.toInt()].latlng.split(",")
                      startRouting("${latlng[0]},${latlng[1]}")*/
                }
                return true
            }
        }

        map.mapEventListener = object : MapEventListener() {
            override fun onMapStable() {
                super.onMapStable()
                if (!isLoadMap) {
                    delegate.onMapLoad()
                    isLoadMap = true
                    initMap()
                }

            }
        }


        view.clear.setOnClickListener {
            clear()
        }

        return view
    }


    private fun addUserMarker(loc: LngLat) {
        val markStCr = MarkerStyleCreator()
        markStCr.size = 20f
        markStCr.bitmap = BitmapUtils.loadBitmapFromAssets("location.png")
        val markSt = markStCr.buildStyle()
        val marker = Marker(loc, markSt)
        userMarkerLayer.clear()
        userMarkerLayer.add(marker)


    }


    /* private fun drawRoutingLine(neshanRouting: MainRoutes) {
         val lineArray = arrayListOf<LngLat>()
         for (route in neshanRouting.routing) {
             val linePoints = PolyUtil.decode(route.line)
             for (i in linePoints.indices) {
                 lineArray.add(LngLat(linePoints[i].longitude, linePoints[i].latitude))
             }
         }
         drawLineGeom(lineArray)
     }

    private fun drawLineGeom(pointList: ArrayList<LngLat>) {
        lineLayer.clear()
        val lngLatVector = LngLatVector()
        for (point in pointList) {
            lngLatVector.add(point)
        }
        val lineGeom = LineGeom(lngLatVector)
        val line = Line(lineGeom, getLineStyle())
        lineLayer.add(line)
        map.setFocalPointPosition(pointList[0], 0.25f)
        map.setZoom(14f, 0f)
        // return lineGeom

    }*/

    private fun getLineStyle(): LineStyle {
        val lineStCr = LineStyleCreator()
        lineStCr.color = ARGB(2, 119, 189, 190)
        lineStCr.width = 8f
        lineStCr.stretchFactor = 0f
        return lineStCr.buildStyle()
    }

    private fun initMap() {
        val bounds = Bounds(
            LngLat(43.505859, 24.647017),
            LngLat(63.984375, 40.178873)
        )
        map.options.setPanBounds(bounds)
        map.setFocalPointPosition(LngLat(51.327650, 35.769368), 1f)
        map.setZoom(14f, 0f)

    }

    fun clear() {
        markerLayer.clear()
    }

    fun addMarker(lat: Double, lng: Double, title: String, color: Int, position: Long) {


        val latDis = SharedPreferencesHelper(activity!!).sharedPreferencesLoad(SharedPreferencesHelper.KEY_GPS_LAT_DIS, "0").toDouble()
        val lngDis = SharedPreferencesHelper(activity!!).sharedPreferencesLoad(SharedPreferencesHelper.KEY_GPS_LNG_DIS, "0").toDouble()

        val newLat = lat - latDis
        val newLng = lng - lngDis

        val markerLocation = LngLat(newLng, newLat)

        val animStBl = AnimationStyleBuilder()
        animStBl.fadeAnimationType = AnimationType.ANIMATION_TYPE_SMOOTHSTEP
        animStBl.sizeAnimationType = AnimationType.ANIMATION_TYPE_SPRING
        animStBl.phaseInDuration = 0.5f
        animStBl.phaseOutDuration = 0.5f
        val animSt = animStBl.buildStyle()
        val markStCr = MarkerStyleCreator()
        markStCr.size = 30f
        markStCr.bitmap = BitmapUtils.loadBitmapFromAssets("ship.png")
        markStCr.animationStyle = animSt
        val markSt = markStCr.buildStyle()
        val marker = Marker(markerLocation, markSt)
        marker.id = position
        markerLayer.add(marker)


        map.setZoom(14f, 1f)
        changeCenter(markerLocation)

    }


    private fun changeCenter(focalPoint: LngLat) {
        //val mashhad = LatLng(lat, lng)
        //googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mashhad, 15f))
        //myMapir.animateCamera(ir.map.sdk_map.camera.CameraUpdateFactory.newLatLngZoom(ir.map.sdk_map.geometry.LatLng(lat, lng), 14.toDouble()))

        map.setFocalPointPosition(focalPoint, 0f)


    }


}
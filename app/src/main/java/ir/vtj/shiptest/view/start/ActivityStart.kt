package ir.vtj.shiptest.view.start

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import com.google.android.material.snackbar.Snackbar
import ir.vtj.shiptest.App
import ir.vtj.shiptest.R
import ir.vtj.shiptest.dao.db.SharedPreferencesHelper
import ir.vtj.shiptest.helper.Utils
import ir.vtj.shiptest.view.operator.ActivityOprator
import ir.vtj.shiptest.view.ship.ActivityShip
import kotlinx.android.synthetic.main.activity_start.*


class ActivityStart : AppCompatActivity() {

    private var doubleBackToExitPressedOnce = false
    private lateinit var snack: Snackbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        tvInfo.text = Utils.getAppVersionAndCreator()

        btnOperator.setOnClickListener {
            val fragmentStarter = FragmentStarter.newInstance(object : FragmentStarter.Interaction {
                override fun onConnect(ip: String, port: Int) {
                    startActivity(Intent(this@ActivityStart, ActivityOprator::class.java).putExtra(ActivityOprator.KEY_IP, ip).putExtra(ActivityOprator.KEY_PORT, port))
                    //finish()
                }


                override fun onError(msg: String) {
                    App.toaster(msg, this@ActivityStart)
                }
            })
            fragmentStarter.show(supportFragmentManager, "startListener")
        }

        btnShip.setOnClickListener {
            startActivity(Intent(this@ActivityStart, ActivityShip::class.java))
            //finish()
        }

        btnSetting.setOnClickListener {
            val builder = AlertDialog.Builder(this@ActivityStart)
            builder.setMessage(getString(R.string.whatsReset))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.drifts)) { dialog, _ ->
                    SharedPreferencesHelper(this@ActivityStart).sharedPreferencesSave(SharedPreferencesHelper.KEY_GPS_LAT_DIS, "0")
                    SharedPreferencesHelper(this@ActivityStart).sharedPreferencesSave(SharedPreferencesHelper.KEY_GPS_LNG_DIS, "0")
                    SharedPreferencesHelper(this@ActivityStart).sharedPreferencesSave(SharedPreferencesHelper.KEY_TIME_DIS, "0")
                    App.toaster(getString(R.string.done), this@ActivityStart)
                    dialog.dismiss()
                }
                .setNegativeButton(getString(R.string.ipporttime)) { dialog, _ ->
                    SharedPreferencesHelper(this@ActivityStart).sharedPreferencesSave(SharedPreferencesHelper.KEY_IP_SHIP, "192.168.1.")
                    SharedPreferencesHelper(this@ActivityStart).sharedPreferencesSave(SharedPreferencesHelper.KEY_PORT_SHIP, "8080")
                    SharedPreferencesHelper(this@ActivityStart).sharedPreferencesSave(SharedPreferencesHelper.KEY_TIMER_SHIP, "20")
                    App.toaster(getString(R.string.done), this@ActivityStart)
                    dialog.dismiss()
                }
                .setNeutralButton(getString(R.string.configs)) { dialog, _ ->
                    SharedPreferencesHelper(this@ActivityStart).sharedPreferencesSave(SharedPreferencesHelper.KEY_CONFIG_VERSION, 0)
                    App.toaster(getString(R.string.done), this@ActivityStart)
                    dialog.dismiss()
                }
            val alert = builder.create()
            alert.show()
        }


    }

    private fun showSnack(str: String) {
        snack = Snackbar.make(relMain, HtmlCompat.fromHtml("<font color=\"#ffffff\">$str</font>", HtmlCompat.FROM_HTML_MODE_LEGACY), Snackbar.LENGTH_SHORT)
            .setAction(getString(R.string.exit)) {
                snack.dismiss()
                overridePendingTransition(R.anim.fadein, R.anim.fadeout)
                finish()
            }.setActionTextColor(Color.YELLOW)
        snack.show()
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            overridePendingTransition(R.anim.fadein, R.anim.fadeout)
            finish()
            return
        }
        this.doubleBackToExitPressedOnce = true
        showSnack(getString(R.string.press_back))
        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, App.EXIT_INTERVAL)
    }

}
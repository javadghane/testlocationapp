package ir.vtj.shiptest.view.operator

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import ir.vtj.shiptest.App
import ir.vtj.shiptest.R
import ir.vtj.shiptest.dao.db.SharedPreferencesHelper
import ir.vtj.shiptest.dao.entity.Config
import ir.vtj.shiptest.dao.entity.Ship
import kotlinx.android.synthetic.main.activity_ship.*
import kotlinx.android.synthetic.main.fragment_list.view.*


@Suppress("DEPRECATION")
class FragmentListData : DialogFragment() {

    private lateinit var delegate: Interaction
    private lateinit var adapterShip: AdapterShip
    private var ships: ArrayList<Ship> = arrayListOf()
    private var address: String = ""

    interface Interaction {
        fun onConfig()
        fun onClear()
        fun onError(msg: String)
        fun onMarkerClick(ship: Ship,position: Int)
        fun onPathClick(ship: Ship,position: Int)
        fun onLoadNewShip(ship: Ship,position: Int)
    }

    companion object {
        fun newInstance(address: String, delegate: Interaction): FragmentListData {
            val frag = FragmentListData()
            val args = Bundle()
            frag.arguments = args
            frag.address = address
            frag.delegate = delegate
            return frag
        }

    }

    fun addShipData(data: String) {
        if (data.length > 10) {
            val configs: ArrayList<Config> = arrayListOf()
            configs.addAll(Config.getConfig(activity!!))
            configs.sortBy { it.order }

            val params = data.split(",")
            if (params.size == 12) {
                val paramSorted: ArrayList<String> = arrayListOf()

                for (conf in configs) {
                    paramSorted.add(params[conf.defOrder - 1])
                }

                val ship = Ship(0, paramSorted[0], paramSorted[1], paramSorted[2], paramSorted[3], paramSorted[4], paramSorted[5], paramSorted[6], paramSorted[7], paramSorted[8], paramSorted[9], paramSorted[10])
                ships.add(ship)
                adapterShip.notifyItemInserted(ships.size - 1)
                view!!.recMain.scrollToPosition(adapterShip.itemCount - 1)
                if (App.isMapShow)
                    delegate.onLoadNewShip(ship,0)
            } else {
                App.toaster(getString(R.string.missData), activity!!)
            }

        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(
            R.layout.fragment_list,
            container, false
        )

        view.tvStatus.text = address



        view.tvConfig.setOnClickListener {
            delegate.onConfig()
        }

        val animation = AnimationUtils.loadAnimation(activity!!, R.anim.fadein)
        adapterShip = AdapterShip(activity!!, animation, ships, object : AdapterShip.Interaction {
            override fun click(ship: Ship, position: Int) {

            }

            override fun onMarkerClick(ship: Ship, position: Int) {
                delegate.onMarkerClick(ship,position)
            }

            override fun onPathClick(ship: Ship, position: Int) {
                delegate.onPathClick(ship,position)
            }

        })

        val mLayoutManager = LinearLayoutManager(activity!!)
        mLayoutManager.reverseLayout = true
        mLayoutManager.stackFromEnd = true
        view.recMain.layoutManager = mLayoutManager

        view.recMain.adapter = adapterShip
        view.tvClear.setOnClickListener {
            ships.clear()
            adapterShip.notifyDataSetChanged()
            delegate.onClear()
        }


        view.edtGpsLatDrift.setText(SharedPreferencesHelper(activity!!).sharedPreferencesLoad(SharedPreferencesHelper.KEY_GPS_LAT_DIS, "0"))
        view.edtGpsLngDrift.setText(SharedPreferencesHelper(activity!!).sharedPreferencesLoad(SharedPreferencesHelper.KEY_GPS_LNG_DIS, "0"))
        view.edtTimeDrift.setText(SharedPreferencesHelper(activity!!).sharedPreferencesLoad(SharedPreferencesHelper.KEY_TIME_DIS, "0"))


        view.tvConfigDrift.setOnClickListener {
            if (view.cardGps.visibility == View.GONE) {
                view.cardGps.visibility = View.VISIBLE
                view.tvConfigDrift.text = getString(R.string.save)

            } else {
                view.cardGps.visibility = View.GONE
                view.tvConfigDrift.text = getString(R.string.driftConfig)
                SharedPreferencesHelper(activity!!).sharedPreferencesSave(SharedPreferencesHelper.KEY_GPS_LAT_DIS, edtGpsLatDrift.text.toString())
                SharedPreferencesHelper(activity!!).sharedPreferencesSave(SharedPreferencesHelper.KEY_GPS_LNG_DIS, edtGpsLngDrift.text.toString())
                SharedPreferencesHelper(activity!!).sharedPreferencesSave(SharedPreferencesHelper.KEY_TIME_DIS, edtTimeDrift.text.toString())
            }
        }




        return view
    }


}
package ir.vtj.shiptest

import android.app.Application
import android.content.Context
import android.graphics.Typeface
import android.widget.Toast
import androidx.multidex.MultiDex
import ir.vtj.shiptest.dao.db.RoomDB

class App : Application() {

    companion object {
        lateinit var db: RoomDB
        lateinit var typeFaceNormal: Typeface
        var configVersion = 1
        lateinit var toast: Toast
        var isMapShow = false
        const val EXIT_INTERVAL: Long = 1000 * 2

        fun toaster(txt: Any, context: Context) {
            toast = Toast.makeText(context, txt.toString(), Toast.LENGTH_SHORT)
            toast.show()
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)

    }

    override fun onCreate() {
        super.onCreate()
        db = RoomDB.getDatabase(applicationContext)
        typeFaceNormal = Typeface.createFromAsset(assets, "yekan.ttf")


        replaceFont()
    }

    private fun replaceFont() {
        val staticField = Typeface::class.java
            .getDeclaredField("MONOSPACE")
        staticField.isAccessible = true
        staticField.set(null, typeFaceNormal)
    }


}
package ir.vtj.shiptest.helper

import android.util.Log
import ir.vtj.shiptest.BuildConfig

class Utils {
    companion object {
        fun log(txt: Any) {
            Log.e("AIS", txt as String)
        }

        fun getAppVersionAndCreator(): String {
            return "نسخه: " + BuildConfig.VERSION_NAME + "\n\n" + "توسط: جواد قانع الحسینی" + "\n\n" + "Hi@JavaDroid.ir"
        }
    }
}
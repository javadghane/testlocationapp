package ir.vtj.shiptest.helper.gps

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import ir.vtj.shiptest.R
import ir.vtj.shiptest.helper.Utils

class HelperGPS {

    private lateinit var locationManager: LocationManager
    private lateinit var lastLocation: Location
    private lateinit var delegate: OnGpsDataChange
    private lateinit var context: Context
    private var speedCalculated = 0.toFloat()

    interface OnGpsDataChange {
        fun onLocationChange(lat: String, lng: String, alt: String, speed: String, way: String)
        fun onSatteliteCountChange(satteliteCount: String)
        fun onPermissionDenied()
        fun onNoGpsAvailable()
        fun onGpsAvailable()
        fun onErrorGps(error: String)
    }

    fun init(context: Context, delegate: OnGpsDataChange): HelperGPS {
        this.delegate = delegate
        this.context = context

        val manager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps()

        } else if (ActivityCompat.checkSelfPermission(context.applicationContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(context.applicationContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
        ) {
            delegate.onGpsAvailable()
            try {
                locationManager = context.getSystemService(AppCompatActivity.LOCATION_SERVICE) as LocationManager
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0L, 0f, locationListener)
            } catch (ex: SecurityException) {
                delegate.onErrorGps(ex.toString())
                Utils.log(ex.toString())
            }

        } else {

            delegate.onPermissionDenied()
        }

        return this
    }


    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {

            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                delegate.onPermissionDenied()
                return
            }


            if (::lastLocation.isInitialized) {
                val elapsedTime = (location.time - lastLocation.time) / 1_000 // Convert milliseconds to seconds
                speedCalculated = lastLocation.distanceTo(location) / elapsedTime
            }
            lastLocation = location
            val newSpeed = if (location.hasSpeed()) location.speed else speedCalculated
            delegate.onLocationChange(location.latitude.toString(), location.longitude.toString(), location.altitude.toString(), newSpeed.toString(), location.bearing.toString())


            var satellites = 0;
            var satellitesInFix = 0;
            locationManager.getGpsStatus(null).timeToFirstFix;

            for (sat in locationManager.getGpsStatus(null).satellites) {
                if (sat.usedInFix()) {
                    satellitesInFix++;
                }
                satellites++;
            }
            delegate.onSatteliteCountChange(satellites.toString())

        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }


    private fun buildAlertMessageNoGps() {
        val builder = AlertDialog.Builder(context)
        builder.setMessage(context.getString(R.string.activeGPS))
            .setCancelable(false)
            .setPositiveButton(context.getString(R.string.yes)) { _, _ ->
                context.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }
            .setNegativeButton(context.getString(R.string.no)) { dialog, _ -> dialog.cancel() }
        val alert = builder.create()
        alert.show()


    }


}
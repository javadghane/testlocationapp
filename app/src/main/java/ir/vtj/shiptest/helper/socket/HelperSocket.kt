package ir.vtj.shiptest.helper.socket

import ir.vtj.shiptest.helper.Utils
import java.io.*
import java.net.ServerSocket
import java.net.Socket
import java.net.UnknownHostException
import java.util.concurrent.Executors


/**
 * Created by JavaDroid at 7/26/2016. modified and refactored at: 14/04/2020
 */
class HelperSocket {
    private var socketIpAddress = ""
    private var socketPort = 8080
    private val socketTimeOut = 1000 * 60 * 2
    private var isConnected = false

    private lateinit var clientSocket: Socket
    private lateinit var serverSocket: ServerSocket
    private lateinit var serverListenerSocket: Socket
    private lateinit var clientThread: Thread
    private lateinit var serverThread: Thread

    lateinit var delegate: SocketListener

    fun initClient(ip: String, port: Int): HelperSocket {
        this.socketIpAddress = ip
        this.socketPort = port
        return this
    }

    fun initServer(port: Int): HelperSocket {
        this.socketPort = port
        return this
    }

    fun isConnect(): Boolean {
        return isConnected
    }

    interface SocketListener {
        fun onConnect()
        fun onError(error: String)
        fun onRead(msg: String)
        fun onDisconnect()
    }

    fun connect(delegate: SocketListener): HelperSocket {
        this.delegate = delegate
        if (!isConnected) {
            clientThread = Thread {
                try {
                    clientSocket = Socket(socketIpAddress, socketPort)
                    clientSocket.soTimeout = socketTimeOut

                    val reader = BufferedReader(InputStreamReader(clientSocket.getInputStream()))
                    val readerStringBuffer = StringBuilder()
                    isConnected = true
                    delegate.onConnect()

                    while (isConnected) {
                        try {
                            if (reader.ready()) {
                                //data have character
                                try {
                                    readerStringBuffer.append(reader.read().toChar())
                                } catch (e: Exception) {
                                    delegate.onError(e.message.toString())
                                    Utils.log("ProblemOnReadData append" + e.message)
                                }
                            } else {
                                //data is end
                                if (readerStringBuffer.isNotEmpty()) {
                                    Utils.log(readerStringBuffer.toString())
                                    delegate.onRead(readerStringBuffer.toString())
                                    try {
                                        readerStringBuffer.delete(0, readerStringBuffer.length)
                                    } catch (e: Exception) {
                                        delegate.onError(e.message.toString())
                                        Utils.log(e.message.toString())
                                    }
                                }
                            }
                        } catch (e: IOException) {
                            delegate.onError(e.message.toString())
                            Utils.log("ProblemOnReadData" + e.message)
                        }
                    }
                } catch (e: IOException) {
                    delegate.onError(e.message.toString())
                    Utils.log("SocketProblemAt connect:" + e.message)
                }
            }
            clientThread.start()
        }
        return this
    }

    fun disconnect(): Boolean {
        isConnected = false
        if (::clientThread.isInitialized && !clientThread.isInterrupted) clientThread.interrupt()
        Utils.log("SocketAndAllObjectCleared")
        if (::delegate.isInitialized)
            delegate.onDisconnect()
        try {
            if (::clientSocket.isInitialized) {
                clientSocket.close()
                clientSocket.shutdownInput()
                clientSocket.shutdownOutput()
                clientSocket.close()
            }
            if (::serverSocket.isInitialized)
                serverSocket.close()
            return true
        } catch (e: IOException) {
            Utils.log(e.message.toString())
        }
        return false
    }


    fun createServer(delegate: SocketListener) {
        Executors.newSingleThreadExecutor().execute {
            try {
                serverSocket = ServerSocket(socketPort)
                serverSocket.reuseAddress = true
                delegate.onConnect()

                while (true) {
                    serverListenerSocket = serverSocket.accept()
                    serverListenerSocket.soTimeout = socketTimeOut

                    serverThread = Thread {
                        try {
                            val readerStringBuffer = StringBuilder()
                            isConnected = true
                            val reader = BufferedReader(InputStreamReader(serverListenerSocket.getInputStream()))


                            while (isConnected) {
                                //Utils.log("onWhile" + reader.toString())
                                try {
                                    if (reader.ready()) {
                                        //data have character
                                        try {
                                            readerStringBuffer.append(reader.read().toChar())
                                        } catch (e: Exception) {
                                            delegate.onError(e.message.toString())
                                            Utils.log("ProblemOnReadData append" + e.message)
                                        }
                                    } else {
                                        //data is end
                                        if (readerStringBuffer.isNotEmpty()) {
                                            Utils.log(readerStringBuffer.toString())
                                            delegate.onRead(readerStringBuffer.toString())
                                            try {
                                                readerStringBuffer.delete(0, readerStringBuffer.length)
                                            } catch (e: Exception) {
                                                delegate.onError(e.message.toString())
                                                Utils.log(e.message.toString())
                                            }
                                        }
                                    }
                                } catch (e: IOException) {
                                    delegate.onError(e.message.toString())
                                    Utils.log("ProblemOnReadData" + e.message)
                                }
                            }
                        } catch (e: IOException) {
                            delegate.onError(e.message.toString())
                            Utils.log("SocketProblemAt connect:" + e.message)
                        }
                    }
                    serverThread.start()

                }


            } catch (ex: java.lang.Exception) {
                delegate.onError(ex.message.toString())
                Utils.log("ErrorOnCreateServer" + ex.message)
            }
        }


    }


    fun sendDataFromServer(data: String) {
        Executors.newSingleThreadExecutor().execute {
            try {
                val out = PrintWriter(BufferedWriter(OutputStreamWriter(serverListenerSocket.getOutputStream())), true)
                out.println(data)
            } catch (e: UnknownHostException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
    }

    fun sendDataFromClient(data: String) {
        Executors.newSingleThreadExecutor().execute {
            try {
                val out = PrintWriter(BufferedWriter(OutputStreamWriter(clientSocket.getOutputStream())), true)
                out.println(data)
            } catch (e: UnknownHostException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
    }
}